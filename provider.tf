provider "aws" {
  region                  = "ap-southeast-2"
  version                 = "~> 2.0"
  profile                 = "ved-devops"
}
