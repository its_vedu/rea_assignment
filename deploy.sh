#!/bin/bash
sudo apt-get update
sudo apt-get install git apache2-utils ruby-bundler nginx -y

sudo cp /dev/null /etc/nginx/sites-available/default
sudo cp /dev/null /etc/nginx/sites-enabled/default
sudo cp /dev/null /etc/nginx/nginx.conf

cat <<EOF | sudo tee -a /etc/nginx/nginx.conf
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
worker_connections 768;
# multi_accept on;
}

http {
        upstream sinatra {
             server localhost:9292;

          }

  server {
listen 80 default_server;
listen [::]:80 default_server;
root /var/www/html;
index index.html index.htm index.nginx-debian.html;
server_name _;
location / {
# First attempt to serve request as file, then
# as directory, then fall back to displaying a 404.
proxy_pass http://sinatra;
try_files $uri $uri/ =404;
                auth_basic "Private Property";
                auth_basic_user_file /etc/nginx/.htpasswd;

}
}
sendfile on;
tcp_nopush on;
tcp_nodelay on;
keepalive_timeout 65;
types_hash_max_size 2048;
include /etc/nginx/mime.types;
default_type application/octet-stream;
ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
ssl_prefer_server_ciphers on;
access_log /var/log/nginx/access.log;
error_log /var/log/nginx/error.log;
gzip on;
include /etc/nginx/conf.d/*.conf;
include /etc/nginx/sites-enabled/*;
}
EOF

PASSWORD=`tr -dc a-z0-9_ < /dev/urandom | head -c 10`
sudo /usr/bin/htpasswd -cb /etc/nginx/.htpasswd nginxuser $PASSWORD
echo Password set to: $PASSWORD
cd /opt/ && git clone https://github.com/rea-cruitment/simple-sinatra-app.git
cd /opt/simple-sinatra-app
bundle install
nohup bundle exec rackup &
sudo systemctl restart nginx

