data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_launch_configuration" "sinatra_launch_config" {
  name          = "sinatra"
  image_id      = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  user_data     = "${file("deploy.sh")}"
  key_name      = "${aws_key_pair.generated_key.key_name}"
  security_groups = ["${aws_security_group.sinatra_sg.id}"]

  
   lifecycle {
    create_before_destroy = true
  }

}


resource "aws_autoscaling_group" "sinatra_asg" {
  name                 = "asg for sinatra app"
  launch_configuration = "${aws_launch_configuration.sinatra_launch_config.name}"
  availability_zones   = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c" ]
  vpc_zone_identifier  = ["${var.subnet}"]
  desired_capacity     = 1
  max_size             = 1
  min_size             = 1

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "sinatra-app"
    propagate_at_launch = true
  }
 
}
