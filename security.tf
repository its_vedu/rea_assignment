resource "aws_security_group" "sinatra_sg" {
  name        = "sinatra_sg"
  description = "Security group for sinatra app"
  vpc_id      = "${var.vpc_id}"

  ingress {
    description = "HTTP from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow port 80 from VPC"
  }
}
