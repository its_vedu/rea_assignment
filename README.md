# rea_assignment

Terraform module to create an AWS ASG with desired count 1 and t2 micro instance type and deploy a simple sinatar application.
The module has been tested on terraform 11.09 and Ubuntu 16.04 LST ( Xenial ).

**Install terraform**

wget https://releases.hashicorp.com/terraform/0.11.9/terraform_0.11.9_linux_amd64.zip

unzip terraform_0.11.9_linux_amd64.zip

mv terraform /usr/local/bin

terraform --version
Terraform v0.11.9


**Running terraform to get the sinatra application.**


git clone https://gitlab.com/its_vedu/rea_assignment.git

cd rea_assignment

Change the below variables in terraform.tfvars and provider.tf 



a) subnet  - VPC private subnet.

b) profile - approprite profile ( usually set in /home/<user>/.aws/credentials file)

c) vpc_id  - approprite vpc id.

d) vpc_cidr - is set to 0.0.0.0/0 . change it to appropiate vpc range or leave it as it is.



After setting all the variables.
terraform init - This will initilize the repo.

terraform plan - This will output all the resource this module is going to create.

terraform apply - This will apply all the changes.


terraform apply will also output the private ip of the ec2-instance on the console, once the apply is complete.

**Check if the app is working.**

Password can be obtained by checking the logs.

aws ec2 get-console-output --instance-id <Instance id created> --profile <Profile> --region ap-southeast-2 | grep -w "Password set to"

curl -u USERNAME  http://PRIVATE IP











