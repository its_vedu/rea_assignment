Terraform was the preferred IAC tool because setting up locally (single GO binary) is easy , it's open source and has good documentation.
Configuration files are written in HCL which is human readable and easy to understand.
Maintaining state and lock can be setup easily ( Not used in this example ).
The Sinatra application is deployed as part of the ec2 userdata. The sinatra application is frontended by Nginx reverse proxy which takes care of proxing as well as the authentication.
