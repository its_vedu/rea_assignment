
variable "key_name" {
  type = "string"
}


variable "subnet" {
  type = "string"
}

variable  "vpc_id" {
  type = "string"
}

variable "vpc_cidr" {
  type = "string"
}
